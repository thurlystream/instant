exports.isProd = process.env.NODE_ENV === 'production'
exports.host = exports.isProd && '192.34.58.86'
exports.ports = {
  http: exports.isProd ? 80 : 9100,
  https: exports.isProd ? 443 : 9101
}
